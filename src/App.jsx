import { useState } from 'react'
import logo from './images/logo.svg'
import mainImg from './images/illustration-working.svg'
import brandIcon from './images/icon-brand-recognition.svg'
import detailedIcon from './images/icon-detailed-records.svg'
import customizableIcon from './images/icon-fully-customizable.svg'
import { AiOutlineMenu } from 'react-icons/ai'
import facebookIcon from './images/icon-facebook.svg'
import twitterIcon from './images/icon-twitter.svg'
import pinterestIcon from './images/icon-pinterest.svg'
import instagramIcon from './images/icon-instagram.svg'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import Navbar from './components/Navbar'

const App = () => {
  const [link, setLink] = useState('')
  const [linkList, setLinkList] = useState([])
  const [shortLinkList, setShortLinkList] = useState([])
  const [isEmptyLink, setIsEmptyLink] = useState(false)

  const handleSubmit = (e) => {
    e.preventDefault()
    if (link.trim() === '') {
      setIsEmptyLink(true)
    } else {
      setLinkList([...linkList, link])
      fetchUrl(link)
      setIsEmptyLink(false)
      setLink('')
    }
  }

  const fetchUrl = async (url) => {
    try {
      const response = await fetch(
        `https://api.shrtco.de/v2/shorten?url=${url}`
      )
      const data = await response.json()
      if (data.ok === false) {
        setLinkList((linkList) => linkList.slice(0, -1))

        throw new Error('There was an error')
      } else {
        setShortLinkList([...shortLinkList, data.result.full_short_link])
      }
    } catch (error) {
      console.log('There was an error ' + error)
    }
  }

  return (
    <main>
      <Navbar />
      <section className="hero">
        <div className="hero-img">
          <img src={mainImg} alt="main-img" />
        </div>
        <div className="hero-center section-center">
          <h1>More than just shorter links</h1>
          <p>
            Build your brand's recognition and get detailed insights on how your
            links are performing.
          </p>
          <button className="btn btn-primary btn-top">Get Started</button>
        </div>
      </section>
      <section className="shortener">
        <form className="section-center form">
          <input
            type="text"
            placeholder="Shorten a link here..."
            value={link}
            onChange={(e) => setLink(e.target.value)}
            className={isEmptyLink ? 'invalid' : ''}
          />
          <p className={isEmptyLink ? 'invalid-text' : ''}>Please add a link</p>
          <button type="submit" className="btn" onClick={handleSubmit}>
            Shorten it!
          </button>
        </form>
      </section>
      <section className="info">
        <div className="info-center section-center">
          <div className="info-links">
            {linkList.map((link, index) => {
              return (
                <div className="info-link" key={index}>
                  <div className="full-link">
                    <p>{link}</p>
                  </div>
                  <div className="short-link">
                    <p>{shortLinkList[index]}</p>
                    <CopyToClipboard text={shortLinkList[index]}>
                      <button className="btn btn-copy">Copy</button>
                    </CopyToClipboard>
                  </div>
                </div>
              )
            })}
          </div>
          <div className="info-title">
            <h1>Advanced Statistics</h1>
            <p>
              Track how your links are performing across the web with our
              advanced statistics dashboard.
            </p>
          </div>
          <div className="info-items">
            <div className="info-item info-item-top">
              <div className="logo-container">
                <img
                  src={brandIcon}
                  alt="item-logo1"
                  className="info-item-logo"
                />
              </div>
              <h2>Brand Recognition</h2>
              <p>
                Boost your brand recognition with each click. Generic links
                don't mean a thing. Branded links help instil confidence in your
                content.
              </p>
            </div>
            <div className="underline"></div>
            <div className="info-item">
              <div className="logo-container">
                <img
                  src={detailedIcon}
                  alt="item-logo2"
                  className="info-item-logo"
                />
              </div>

              <h2>Detailed Records</h2>
              <p>
                Gain insights into who is clicking your links. Knowing when and
                where people engage with your content helps inform better
                decisions.
              </p>
            </div>
            <div className="underline"></div>
            <div className="info-item info-item-bottom">
              <div className="logo-container">
                <img
                  src={customizableIcon}
                  alt="item-logo3"
                  className="info-item-logo"
                />
              </div>
              <h2>Fully Customizable</h2>
              <p>
                Improve brand awareness and content discoverability through
                customizable links, supercharging audience engagement.
              </p>
            </div>
          </div>
        </div>
      </section>
      <section className="boost">
        <div className="boost-center section-center">
          <h1>Boost your links today</h1>
          <button className="btn btn-primary">Get Started</button>
        </div>
      </section>
      <footer>
        <div className="footer-center section-center">
          <div className="footer-logo">
            <h1>Shortly</h1>
          </div>
          <div className="footer-links">
            <ul>
              <li>Features</li>
              <li>Link Shortening</li>
              <li>Branded Links</li>
              <li>Analytics</li>
            </ul>
            <ul>
              <li>Resources</li>
              <li>Blog</li>
              <li>Developers</li>
              <li>Support</li>
            </ul>
            <ul>
              <li>Company</li>
              <li>About</li>
              <li>Our Team</li>
              <li>Careers</li>
              <li>Contact</li>
            </ul>
          </div>
          <div className="footer-social">
            <a href="https://www.facebook.com/">
              <img src={facebookIcon} alt="facebookIcon" />
            </a>
            <a href="https://www.twitter.com/">
              <img src={twitterIcon} alt="twitterIcon" />
            </a>
            <a href="https://www.pinterest.com/">
              <img src={pinterestIcon} alt="pinterestIcon" />
            </a>
            <a href="https://www.instagram.com/">
              <img src={instagramIcon} alt="instagramIcon" />
            </a>
          </div>
        </div>
      </footer>
    </main>
  )
}
export default App
