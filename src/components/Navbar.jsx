import React, { useState, useRef, useEffect } from 'react'
import { FaBars } from 'react-icons/fa'
import logo from '../images/logo.svg'

const links = [
  {
    id: 1,
    url: '/',
    text: 'features',
  },
  {
    id: 2,
    url: '/',
    text: 'pricing',
  },
  {
    id: 3,
    url: '/',
    text: 'ressources',
  },
]

const connect = [
  {
    id: 1,
    url: '/',
    text: 'login',
  },
  {
    id: 2,
    url: '/',
    text: 'sign up',
  },
]

const Navbar = () => {
  const [showLinks, setShowLinks] = useState(false)
  const linksContainerRef = useRef(null)
  const linksRef = useRef(null)
  const toggleLinks = () => {
    setShowLinks(!showLinks)
  }
  useEffect(() => {
    const linksHeight = linksRef.current.getBoundingClientRect().height
    if (showLinks) {
      linksContainerRef.current.style.display = `block`
      linksContainerRef.current.style.height = `300px`
    } else {
      linksContainerRef.current.style.display = 'none'
    }
  }, [showLinks])
  return (
    <nav>
      <div className="nav-center">
        <div className="nav-header">
          <img src={logo} className="nav-logo" alt="logo" />
          <button className="nav-toggle" onClick={toggleLinks}>
            <FaBars />
          </button>
        </div>
        <div className="links-container" ref={linksContainerRef}>
          <ul className="links" ref={linksRef}>
            {links.map((link) => {
              const { id, url, text } = link
              return (
                <li key={id}>
                  <a href={url}>{text}</a>
                </li>
              )
            })}
            <div className="underline-connect"></div>
            <a href="/">Login</a>
            <button className="btn btn-primary">Sign Up</button>
          </ul>
        </div>
        <ul className="connect">
          {connect.map((connectButton) => {
            const { id, url, icon } = connectButton
            return (
              <li key={id}>
                <a href={url}>{icon}</a>
              </li>
            )
          })}
        </ul>
      </div>
    </nav>
  )
}

export default Navbar
